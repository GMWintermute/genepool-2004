<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=17;
$page_title="Genomic Sequence for  ";
$page_title.=$gene_id;

require("popup_header.php");

if (!isset($build))
  {
    echo 'Usage: gene.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }
  
$syscmd = "/genepool/src/octseqmaker.pl -i /genepool/gpdata/builds/".$build."/symlinks/".$chr_id;
$syscmd .= "/".$seq_id.".out.sup -c ".$chr_id." -v ".$gene_id." -b ".$build;

//print "$syscmd<BR>";
system($syscmd);