<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=1;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: contig.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }


mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);

$sql = "select begin,end,strand from sequence where seq_id = '$seq_id'";
$result = mysql_query($sql);
$row = mysql_fetch_row($result);
?>

<table cellspacing="0" cellpadding="02" border="0" width="100%">
<tr>
    <th align="center">Contig</th>
    <th align="center">Chromosome</th>
    <th align="center">Begin</th>
    <th align="center">End</th>
    <th align="center">Length</th>
    <th align="center">Strand</th>
</tr>
<tr><td align="center"><?echo $seq_id?></td><td align="center"><?echo $chr_id?></td><td align="center"><?echo $row[0]?></td>
<td align="center"><?echo $row[1]?></td><td align="center"><?echo ($row[1]-$row[0])?></td><td align="center"><?echo $row[2]?></td></tr>
<tr><td colspan = "6"  height="1"><img src="../images/760_trans_spacer.gif"></td></tr>
</table><br><center>EXP Genes Modeled using ESTs+cDNAs</center><br>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <th align="center">ID</th>
    <th align="center">Num</th>
    <th align="center">Var</th>
    <th align="center">Str</th>
    <th align="center">Begin</th>
    <th align="center">End</th>
    <th align="center">Exons</th>
    <th align="center">Prot Len</th>
    <th align="center">Num Evid</th>
    <th align="center">GP Score</TH>
    <th align="center" width = "35%">Product</th>
   
</tr>


<?
// Load All Exp Genes for this contig
//$sql="select * from chr_".$chr_id."_summary where seq_id = '$seq_id' order by gene_number, variant_number";
$sql="select gene_id, gene_number, variant_number, strand, gene_begin, gene_end, number_exons, protein_length, number_evidences,gpscore, product from chr_".$chr_id."_summary where seq_id = '$seq_id' order by gene_number, variant_number";
$result = mysql_query($sql) or must_die(mysql_error());

//debugging block
//$row = mysql_fetch_row($result);
//for ($k=0; $k <= sizeof($row); $k++)
//  echo $k.": ".$row[$k]."\n";
//exit;


$color_count=0;
$color_array[1] = "#FFE4C4";
$color_array[2] = "#FFFFFF";

for ($k = 0; $row = mysql_fetch_row($result); $k++)
  {

 	if ($row[2] == 1)
	  $color_count++;
	if ($color_count > 2)
	  $color_count = 1;



	  echo '<tr bgcolor="'.$color_array[$color_count].'" align=\'center\'><td width="44">';
	  $product=$row[10];
        build_gene_link($row[4],$chr_id,"2",$row[0],"70",$build,$product);
	echo '</td>';
$gene_id=$row[0];

?>

<td align="center"><?echo $row[1]?></td>
<td align="center"><?echo $row[2] ?></td>
<td align="center"><?if ($row[3]=="f") echo "Forward"; elseif ($row[3]=="r") echo "Reverse"; else echo $row[3]; ?></td>
<td align="center"><?echo $row[5] ?></td>
<td align="center"><?echo $row[4] ?></td>
<td align="center"><?echo $row[6] ?></td>
<td align="center"><?echo $row[7] ?></td>
<td align="center"><?echo $row[8] ?></TD>
<td align= "center"><?echo $row[9]?></TD>
														      
<td align="left"><?if ($product != "" ) {echo $product; } else echo "&nbsp;&nbsp;"; ?></td>
</tr>
<?
//echo '</td></tr>';
  }
echo '<tr><td height ="1" colspan="11"><img src="../images/760_trans_spacer.gif"></td></tr>';
echo '</table>';
//mysql_free_result($evidresult);
mysql_free_result($result);


require("nav_end.php");
?>
