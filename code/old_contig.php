<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=1;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: contig.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }


mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);

$sql = "select begin,end,strand from sequence where seq_id = '$seq_id'";
$result = mysql_query($sql);
$row = mysql_fetch_row($result);
?>

<table cellspacing="0" cellpadding="02" border="0" width="100%">
<tr>
    <th align="center">Contig</th>
    <th align="center">Chromosome</th>
    <th align="center">Begin</th>
    <th align="center">End</th>
    <th align="center">Length</th>
    <th align="center">Strand</th>
</tr>
<tr><td align="center"><?echo $seq_id?></td><td align="center"><?echo $chr_id?></td><td align="center"><?echo $row[0]?></td>
<td align="center"><?echo $row[1]?></td><td align="center"><?echo ($row[1]-$row[0])?></td><td align="center"><?echo $row[2]?></td></tr>
<tr><td colspan = "6"  height="1"><img src="../images/760_trans_spacer.gif"></td></tr>
</table><br><center>EXP Genes Modeled using ESTs+cDNAs</center><br>
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <th align="center">ID</th>
    <th align="center">Num</th>
    <th align="center">Var</th>
    <th align="center">Str</th>
    <th align="center">Begin</th>
    <th align="center">End</th>
    <th align="center">Exons</th>
    <th align="center">Prot Len</th>
    <th align="center">Num Evid</th>
    <th align="center" width = "35%">Product</th>
   
</tr>


<?
// Load All Exp Genes for this contig
//$sql="select * from chr_".$chr_id."_summary where seq_id = '$seq_id' order by gene_number, variant_number";
$sql="select gene_id, gene_number, variant_number, strand, gene_begin, gene_end, number_exons, protein_length, number_evidences,product from chr_".$chr_id."_summary where seq_id = '$seq_id' order by gene_number, variant_number";
$result = mysql_query($sql) or must_die(mysql_error());

//debugging block
//$row = mysql_fetch_row($result);
//for ($k=0; $k <= sizeof($row); $k++)
//  echo $k.": ".$row[$k]."\n";
//exit;



for ($k = 0; $row = mysql_fetch_row($result); $k++)
  {
    if ($k % 2 == 0)
	  echo '<tr bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFE4C4\'" align=\'center\' class =\'second\'><td width="44">';
	else
	  echo '<tr  bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFFFFF\'" align=\'center\'><td width="44">';
	  $product=$row[9];
        build_gene_link($row[4],$chr_id,"2",$row[0],"70",$build,$product);
	echo '</td>';
$gene_id=$row[0];

?>

<td align="center"><?echo $row[1]?></td>
<td align="center"><?echo $row[2] ?></td>
<td align="center"><?if ($row[3]=="f") echo "Forward"; elseif ($row[3]=="r") echo "Reverse"; else echo $row[3]; ?></td>
<td align="center"><?echo $row[5] ?></td>
<td align="center"><?echo $row[4] ?></td>
<td align="center"><?echo $row[6] ?></td>
<td align="center"><?echo $row[7] ?></td>
<td align="center"><?echo $row[8] ?></TD>

<td align="left"><?if ($product != "" ) {echo $product; } else echo "&nbsp;&nbsp;"; ?></td>
</tr>
<?
//if ($k % 2 == 0)
//   echo '<tr bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFE4C4\'" align=\'center\' class =\'second\'>';
//else
//   echo '<tr  bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFFFFF\'" align=\'center\'>';
//echo '<td colspan="10" align="center">';
//if ($product !="")
//   echo '<font size = "-1">'.build_blast_link($build,$chr_id,$seq_id,$row[0],$product).'</font>';
//else
//   echo '<font size = "-1">No Hits Found</font>';
echo '</td></tr>';
  }
echo '<tr><td height ="1" colspan="10"><img src="../images/760_trans_spacer.gif"></td></tr>';
echo '</table>';
//mysql_free_result($evidresult);
mysql_free_result($result);


require("nav_end.php");
?>
