<?session_start();
//session_register("build");
if (!isset($chr_id))
  $chr_id=17;
$page_title="GenePOOL Select Gene for build ";
$page_title.=$build;
$page_title.=" Contig ";
$page_title.=$seq_id;
require("nav_begin.php");

if (!isset($build))
  {
    echo 'Usage: gene.php?chr_id=1&build=testgenomix&seq_id=NT_004488.7';
	exit;
  }


mysql_connect($db_host,$db_username,$db_password);
mysql_select_db($build);


//grab the number of distinct gene_id with products mactching search string
$sql = "select distinct(gene_id),chr_id from master_products where product like '%".$search_string."%'";
//echo '<BR>'.$sql.'<BR>';
$result = mysql_query($sql);

//$distinct_count = mysql_num_rows($result);

$seq_hash = array ();


//now lets grab the number of isoforms
//we will do this by grabbing the sequence id, and gene number 
$count = 0;
echo '<table cellpadding=0 cellspacing=0 border=0 width=50%><TR><TH colspan=5>Search results for '.$search_string.'</TH></TR><TR><TH>Contig</TH><TH>Gene Number</TH><TH>Number Isoforms</TH><TH>Chr Id</TH><TH>Novel</TH></Tr>';
$k=0;
$novelcnt = 0;

while ($row = mysql_fetch_row($result))
{
  $sql = "select seq_id, gene_number from chr_".$row[1]."_summary where gene_id = '".$row[0]."'";
  // echo '<br>'.$sql.'<br>';

  $generesult = mysql_query($sql);
  
  while(  $generow = mysql_fetch_row($generesult))
    {
      $hash_id = $generow[0]."_".$generow[1];
      // echo 'hash_id: '.$hash_id.'<BR>';
      $found = 0;
      for ($a=0; $a <= sizeof($seq_hash); $a++)
	{
	  if ($seq_hash[$a] == $hash_id)
	    $found = 1;
	  //print "FOUND<BR>";
	
	}
      if (!$found)
	{ 
	  $k++;
	 
	  
	  if ($k % 2 == 0)
	    {
	      echo '<TR class="second" bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFE4C4\'" align=\'center\' class =\'second\'><TD>'; 
	    }
	  else
	    {
	      echo '<TR bgcolor="#FFFFFF" onmouseover="javascript:style.background=\'#DAA520\'" onmouseout="javascript:style.background=\'#FFFFFF\'" align=\'center\'><TD>';
	    }
	  $sql = "select gene_id, novel from chr_".$row[1]."_summary where seq_id like '".$generow[0]."' and gene_number = '".$generow[1]."' and variant_number = '1'";
	  // echo $sql."<BR><BR>";
	  $tmpresult = mysql_query($sql);
	  $tmprow = mysql_fetch_row($tmpresult);
	  if ($tmprow[1] == 1)
	    {
	      $novelcnt++;
	      $novel = "Yes";
	    }
	  else
	    {
	      $novel = "No";
	    }
	  build_gene_search_link($generow[0],$row[1],0,$tmprow[0],0,$build,0);
	  echo "</TD><TD align=center>".$generow[1]."</TD><TD align=center> ";
	  //	   echo $generow[0]."</TD><TD align=center>".$generow[1]."</TD><TD align=center> ";
	  $sql = "select count(*) from chr_".$row[1]."_summary where seq_id like '".$generow[0]."' and gene_number = '".$generow[1]."'";
	  // echo '<br>'.$sql.'<br>';
	  $countresult = mysql_query($sql);
	  $countrow = mysql_fetch_row($countresult);
	  $count += $countrow[0];
	  echo $countrow[0]."</TD><TD align=center>".$row[1]."</TD><TD>".$novel."</td></TR>";
	  array_push($seq_hash,$hash_id);
	}


    }
} 
echo '</table><BR><BR>';
echo '<font size="+1"><B>There are '.sizeof($seq_hash).' '.$search_string.' results<BR>';
echo '<BR>There are '.$count.' isoforms for '.$search_string.'<BR>';
echo '<BR>There are '.$novelcnt.' Novels</B></font>';

//echo '<BR><BR>Hash Table<BR>';
//for ($a=0; $a <= sizeof($seq_hash); $a++)
//	{
//	  print $seq_hash[$a]."<br>";
//
//	
//	}


