#!/usr/bin/perl
#  Script name: exp2fastalog.pl -c 1 -i [file name]
#               exp2fastalog.pl -c 1 -i /in/path/NT_011516.5_exp.out
#  Date:        11/05/2001
#  programmers: Mary E. Perkins 
#  Description:  
 #--------------------------------------------------------------------------------




$genomixdb=$ENV{GENOMIXDB};  
$ARGC = @ARGV;

#check for appropriate number of options
if ($ARGC < 2 )
  {
   print_usage();
    exit 0;
  }
 
#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
  {
    if ($ARGV[$a] =~ /-i/)
      { $input = $ARGV[$a+1]; }
    if ($ARGV[$a] =~ /-c/)
      { $chromo = $ARGV[$a+1]; } 	
  }
                          
$basedir = $input;    #the base subdirectory from the -i option
@inputarray = split(/\//,$input);
$inputlen = @inputarray;     
@arraycontig = split(/\./,$inputarray[$inputlen-1]); 		     
@arraynum = split(/\_/,$arraycontig[1]);   	

$newname = $arraycontig[0];
$newname .= ".";
$newname.=  $arraynum[0];       #contig number

@logarray =split(/\./,$input);  
$loginput = "$logarray[0].";
$loginput .= $logarray[1];
$loginput .= ".log";

if (open(INFILE,"< $input"))     #open input file
   {
 #      print "input file opened $input\n";
   }
else
   {
      print "Error opening $input\n";
      exit 0;	
   }



if (open(LOGINPUT,"< $loginput"))     #open input file
   {
#       print "log input file opened $loginput\n";
   }
else
   {
      print "Error opening $loginput\n";
      exit 0;	
   }     

if (open(ERRORLOG,">> error.list"))     #open output file
   {
 #      print "error.list file opened \n";
   }
else
   {
      print "Error opening error.list\n";
      exit 0;
   }           

             #do while there is input
             while ($pass=<INFILE>)
             {
                if ($pass =~ /end genes/)   #find the words 'end genes' 
                {
                  $passnew=<LOGINPUT>;
                  
                  	if ($passnew=~/error/)
                  	{
				print ERRORLOG "CHR_$chromo/$newname\n";
                  	}
		  	elsif ($passnew=~/fail/)
		  	{
				print ERRORLOG "CHR_$chromo/$newname\n";
		  	}
		  
                  
                }
		else
		{
			$passnew=<LOGINPUT>;           
		       	if ($passnew=~/error/)
       	        	{
       	               		print ERRORLOG "CHR_$chromo/$newname\n";
				last;
       	        	}
       	        	else
			{
				if ($passnew=~/fail/)
       	             		{
       	                       		print ERRORLOG "CHR_$chromo/$newname\n";
					last;
       	               		}
			}                  
		}       
             }

exit 1;



###############################
#Subroutines start here       #
###############################


sub print_usage()
  {
    print "========================================================\n";
    print "exp2fastalog  (c)Copyright 2001, by Genomix Corporation\n";
    print "========================================================\n";
    print "Usage: $0 -i inputfile - c chromosome number \n";
        print <<  "end_tag";
Options:
 
-i		input file (EXP3 or EXP5 output file)

-c		chromosome number

example:
	exp2fastalog -c 1 -i /in/path/NT_0001234.5_exp.out > /out/path/error.list
end_tag
  }
                                                        

