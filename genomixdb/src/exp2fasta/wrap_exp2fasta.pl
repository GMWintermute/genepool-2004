#!/usr/bin/perl
# Script name: wrap_exp2fasta.pl -i [Chromsome subdirectory to start in]
#		example: perl wrap_exp2fasta.pl -i /home/mary/stuff/CHR_01

# Date:        11/16/2001
# Programmer:  Mary E. Perkins
#-----------------------------------------  

$genomixdb=$ENV{GENOMIXDB};   
$ARGC = @ARGV;
 
#check for appropriate number of options
if ($ARGC < 2 )
  {
   print_usage();
    exit 1;
  }
 
#define variable here from command line.
for ($a=0; $a<=$ARGC; $a++)
  {
    if ($ARGV[$a] =~ /-i/)
      { $basedir = $ARGV[$a+1]; }

  }                                    



@arraychromo=split(/\//,$basedir);
$testlen=@arraychromo;
$cnt=$testlen-1;
$chromo=$arraychromo[$cnt];
@arraychromo=split(/\_/,$chromo);
$chromo=$arraychromo[1];          #get the chromosome number
 
#get the file folder
$folder = $basedir;


 
#open the subdirectory and find out how many files are there
#place the files into an array named @dirlist
opendir(MYDIR,$folder);
@dirlist=readdir(MYDIR);
closedir(MYDIR);
$numfiles = @dirlist;   #number of files in the subdirectory
 

#loop thru all files in the directory
for ($count =2; $count < $numfiles; $count++)
{
    #get the folder and file name for input
    $filename=$folder;
    $filename.="/";
    $filename.=$dirlist[$count];
 
 
 
    #get the contig number for output
 
    @arrayfilename=split(/\./,$dirlist[$count]);
 
    #Only make fasta data out of .out files
    $holder=$arrayfilename[2];                 
 if ($holder eq "out")
	{
    
	$rc = 0xffff & system("exp2fasta.pl -u -m -c $chromo -i $filename >/data/GenomixDB/build_001/fbseqs/CHR_$chromo.mrna.fsa");
	$rc = 0xffff & system("exp2fasta.pl -u -p -c $chromo -i $filename >/data/GenomixDB/build_001/fbseqs/CHR_$chromo.prot.fsa"); 
	if ($rc == 0)
	{print "exp2fasta.pl failed\n";}
	else
	{print "exp2fasta.pl passed\n";}

	
	}

}     #for $count loop


###############################
#Subroutines start here       #
###############################
 
 
sub print_usage()
  {
    print "========================================================\n";
    print "wrap_exp2fasta.pl  (c)Copyright 2001, by Genomix Corporation\n";
    print "========================================================\n";
    print "Usage: $0 -m mrna -p protein -i inputfile - c chromosome number \n";
        print <<  "end_tag";
Options:
 
-i		chromosome subdirectory to start in
 
 
example:
        wrap_exp2fasta  -i /in/path/CHR_01 
end_tag
  }
 

