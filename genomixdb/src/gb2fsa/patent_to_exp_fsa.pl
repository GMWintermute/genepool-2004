#!/usr/bin/perl

use POSIX;

#Ok, lets grab the filename from the command line, open it and get ready to process it.

$filename = $ARGV[0];
$tissue_type="";
$organism="";
$description="";
$seq="";


if (!open(INFILE,"<$filename"))
  {
    print "Could not open $filename";
    exit(1);
  }
$first_entry=1;
while ( $line = <INFILE>)
  {
    if ($line =~ /^Sv   /)
      {process_entry();}

  }

close(INFILE);

sub process_entry()
  {
    $tissue_type="";
    $organism="";
    $description="";
    $seq="";

    #grab the id number
    chomp($line);
    $line =~ s/\;//g;
    @idarray = split(" ",$line);
    $sp_id = $idarray[1];
    $protein_length = $idarray[4];
    #print "SWISSPROT ID: $sp_id\n";

    do {
          $line = <INFILE>;
          chomp($line);
          if ($line =~ /^RC   / && $line =~ /TISSUE/)
            { #process the tissue line
              $line =~ s/\;//g;
              @tmparray = split(" ",$line);
              $tmpsize = @tmparray;
              for ($a=0; $a <= $tmpsize; $a++)
                { #print "tmparray[$a] $tmparray[$a]\n";
                 if ($tmparray[$a] =~ /TISSUE/)
                   {  $tmparray[$a]=~ s/\;/=/g;
                      @tmp3array = split("=",$tmparray[$a]);
                      $tissue_type .= $tmp3array[1];
                      $tissue_type =~ s/ /_/g;
                      $tissue_type =~ s/\;//g;
                   }
                } #end for array
              #print "Tissue: $tissue_type\n";
            } #end tissue line
          if ($line =~ /^DE   /)
            {
              do
                {
                   chomp($line);
                   $line =~ s/[\'\(\)\/\\]//g;
                   @tmparray = split("DE   ",$line);
                   $description .= $tmparray[1];  #COULD BE 1
                   $line = <INFILE>;

                } while ($line != /^GN   /);
              #print "Description: $description\n";
            }
          if ($line =~ /^OS   /)
            { #process the Organism
               chomp($line);
               $line=~ s/\.//;
               @tmparray = split("OS   ",$line);
               #print "$line\n";
               $tmparray[1] =~ s/\(/_/g;
               @tmp4array = split("_",$tmparray[1]);
               $organism = $tmp4array[0];
               #print "ORGANISM: $organism\n";
               if ($organism =~ /Rattus norvegicus/)
                 { $organism = "rat"; }
               if ($organism =~ /Mus musculus/)
                 { $organism = "mouse"; }
               if ($organism =~ /Homo sapiens/)
                 { $organism = "human"; }
               $organism =~ s/ /_/g;
               $organism =~ s/\.//g;
               #print "ORGANISM: $organism\n";
           } #end organism line

          if ($line =~ /^SQ   /)
            {
              #advance the filepos by 1 line
              $line=<INFILE>;
                do
                {
                   chomp($line);
                   #print "SEQUENCE Found\n";
                   $line=~ s/^SQ   //;
                   $line =~ s/ //g;
                   $line =~ s/[0-9]//g;
                   $seq .= $line;

                   $line = <INFILE>;
                   $seq_length = length($seq);
                   #print "Sequence length: $seq_length\n";
                } while ($line !~ /^\/\//);
                #print "Master SEQ: $seq\n";


            }

        } while ($line !~ /^\/\//);
      #output entry here
      if ($tissue_type eq "")
        { $tissue_type="no_tissue_information"; }
      #print "Output Routine started\n";

      print ">$organism|$sp_id";
    print "_pat|embl_pat|$tissue_type|$description\n";
      $length_of_sequence = length($seq);
      #print "length_of_seq: $length_of_sequence\n";
      @seq_array=split("",$seq);
      $count =0;
      for($z=0; $z <= $length_of_sequence; $z++)
        {
          print $seq_array[$z];
          $count++;
          if ($count == 59)
            { print "\n"; $count = 0;}
        }
      #print "end seq\n";
      print "\n";


  }
