#!/usr/bin/perl

###################################
# gdxINDEX
#
# Script to index a database for
# use with GrailEXP.
# Database must contain the accession
# number in the second field, delimited
# by |, e.g. <blah>|<accession no>|<blah>.....
###################################
$genomixdb=$ENV{GENOMIXDB};
if(!$genomixdb) {die "GENOMIXDB environment variable not set\n"; }
if (open(CFGFILE,"$genomixdb/cfg/genomixdb.cfg"))
  {
    while ($line=<CFGFILE>)
      {
 	if ($line =~ /refseq_gbff/)
           {
             @tmparray = split("=",$line);
             $refseq_gbff = $tmparray[1];
	     chomp($refseq_gbff);
             #print "$refseq_gbff\n";
           }
        if ($line =~ /refseq_gdx/)
           {
             @tmparray = split("=",$line);
             $refseq_gdx = $tmparray[1];
             chomp($refseq_gdx);
             #print "$refseq_gdx\n";
           }
      }
  }
close(CFGFILE);
$usage = "\nUsage: $0 <database>\n";
$pad = "################";

if(@ARGV != 1) { die $usage; }
$tmpdb = shift;

$db = "$refseq_gbff/$tmpdb";
$gdx = "$refseq_gdx/$tmpdb";
$gdx .="_accession.gdx";
print "db: $db\ngdx: $gdx\n";








open GDXF, ">$gdx" or die "\n[$0]: Couldn't open $gdx for writing.\n$usage\n\n";

open DBF, $db or die "\n[$0]: Couldn't open database $db for reading!\n$usage";
$ptr = tell DBF;
while($line = <DBF>) {
  if($line =~ /^LOCUS/) {
    $locusptr = tell DBF;
    $startptr = $locusptr - length($line); }
  if($line =~ /^ACCESSION/)
   {
    @info = split /\s+/, $line;
    $tmpacc = $info[1];
    #print "$tmpacc\n";
    #$tmpacc =~ s/GI://;
    #print "$tmpacc\n";
    #print "Startptr = $startptr\n";
    $acc = $tmpacc.(substr($pad, 0, 16-length($tmpacc)));
    #print "Acc = $acc\n";
    push @keys_uns, $acc;
    $offset="";
    $offst = sprintf("%012s", $startptr);
    #print "Offset = $offst\n";
    $offsets{$acc} = $offst;
  }
  #$ptr = tell DBF;
}
close DBF;

@keys = sort { $a cmp $b } @keys_uns;

for($i = 0; $i < @keys; $i++) { print GDXF "$keys[$i]$offsets{$keys[$i]}"; }

close GDXF;

exit 0;
