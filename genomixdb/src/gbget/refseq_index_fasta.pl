#!/usr/bin/perl

###################################
# gdxINDEX
#
# Script to index a database for
# use with GrailEXP.
# Database must contain the accession
# number in the second field, delimited
# by |, e.g. <blah>|<accession no>|<blah>.....
###################################
$genomixdb=$ENV{GENOMIXDB};
if(!$genomixdb) {die "GENOMIXDB environment variable not set\n"; }
if (open(CFGFILE,"$genomixdb/cfg/genomixdb.cfg"))
  {
    while ($line=<CFGFILE>)
      {
 	if ($line =~ /refseq_fsa/)
           {
             @tmparray = split("=",$line);
             $refseq_fsa = $tmparray[1];
             $refseq_fsa =~ s/\s+//;
	     chomp($refseq_fsa);
             chomp($refseq_fsa);
             print "refseq_fsa: $refseq_fsa*stop\n";
           }
        if ($line =~ /refseq_gdx/)
           {
             @tmparray = split("=",$line);
             $refseq_gdx = $tmparray[1];
             chomp($refseq_gdx);
             chomp($refseq_gdx);
             #print "refseq_gdx: $refseq_gdx\n";
           }
      }
  }
close(CFGFILE);


$usage = "\nUsage: $0 <database>\n";
$pad = "################";

#$indexdir="/data/sequences/genbank/gdx/indexes/";
if(@ARGV != 1) { die $usage; }
$tmpdb = shift;
chomp($tmpdb);
#print "tmpdb: $tmpdb\n";
$db = "$refseq_fsa";
$db .= "/$tmpdb";
$gdx = "$refseq_gdx/$tmpdb";
$gdx .="_fsa.gdx";
#print "db: $db\ngdx: $gdx\n";

open GDXF, ">$gdx" or die "\n[$0]: Couldn't open $gdx for writing.\n$usage\n\n";

open DBF, $db or die "\n[$0]: Couldn't open database $db for reading!\n$usage";
$ptr = tell DBF;
while($line = <DBF>) {
  if($line =~ /^>/) {
    
    @info = split /\|/, $line;
    #$numinfo=@info;
    #for ($b=0; $b <= $numinfo; $b++)
    #  { print "$b: $info[$b]\n"; }
    $tmpacc = $info[1];
    #print "key: $tmpacc\n";
    $acc = $tmpacc.(substr($pad, 0, 16-length($tmpacc)));
    push @keys_uns, $acc;
    $offst = sprintf("%012s", $ptr);
    $offsets{$acc} = $offst;
  }
  $ptr = tell DBF;
}
close DBF;

@keys = sort { $a cmp $b } @keys_uns;

for($i = 0; $i < @keys; $i++) { print GDXF "$keys[$i]$offsets{$keys[$i]}"; }

close GDXF;

exit 0;
