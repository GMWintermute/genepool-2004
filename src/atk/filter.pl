#!/usr/bin/perl

# Genomix Corporation
# July 26, 2002
# This tool inputs a raw alignment file and outputs only those lines that meet certain criteria.
# Note that it also removes dashes if the alignments that supported the dashes were deleted.

$min_size = 0;
$del_file = "";
%del_hash = ();
$ins_file = "";
%ins_hash = ();
for (my $i = 0; $i < scalar(@ARGV) - 1; $i++)
{
  if ($ARGV[$i] =~ /^-m/) {$min_size = $ARGV[$i+1];}
  elsif ($ARGV[$i] =~ /^-d/) {$del_file = $ARGV[$i+1];}
  elsif ($ARGV[$i] =~ /^-i/) {$ins_file = $ARGV[$i+1];}
  elsif ($ARGV[$i] =~ /^-/)
  {
    print STDERR "Unknown option $ARGV[$i] (must be -m -d or -i)\n";
    exit;
  }
}

# Read in deletion and insertion files
if ($del_file ne "")
{
  open(DEL, "< $del_file") or die "Unable to open deletion list file $del_file\n$!\n";
  while (my $line = <DEL>) {chomp $line; $del_hash{$line} = 1;}
}
close DEL;

if ($ins_file ne "")
{
  open(INS, "< $ins_file") or die "Unable to open insertion list file $ins_file\n$!\n";
  while (my $line = <INS>) {chomp $line; $ins_hash{$line} = 1;}
}
close INS;

while (my $line = <STDIN>)
{
  chomp $line;
  my @parts = split(/\s+/, $line);

  # First, automatically delete or insert alignments in the deletion or
  # insertion lists
  if ($del_hash{$parts[1]} == 1) {next;}
  if ($ins_hash{$parts[1]} == 1)
  {
    push @aligns, $parts[0];
    push @labels, $parts[1];
    next;
  }

  # Now, check to see if the alignment, letters ONLY, is big enough
  my $letters = $parts[0];
  $letters =~ s/(\.|\-)//g;
  if (length($letters) >= $min_size) {push @aligns, $parts[0]; push @labels, $parts[1];}
}

# Now, go through and delete unsubstantiated dashes
for (my $i = 0; $i < length($aligns[0]); $i++)
{
  if (substr($aligns[0], $i, 1) eq "-")
  {
    my $DASH_OKAY = 0;
    foreach my $al (@aligns)
    {
      if ((substr($al, $i, 1) ne "-") && (substr($al, $i, 1) ne ".")) {$DASH_OKAY = 1; break;}
    }

    if ($DASH_OKAY == 0)
    {
      for (my $j = 0; $j < scalar(@aligns); $j++) {substr($aligns[$j], $i, 1) = "";}
      $i--;
    }
  }
}

# Now, print out the lines
for (my $i = 0; $i < scalar(@aligns); $i++) {print "$aligns[$i] $labels[$i]\n";}
