#!/usr/bin/perl

use POSIX;

# September 12, 2002 - allow user to input a file of alternate accession numbers (labels for the
# sequences)
@alt_acc = ();
if (scalar(@ARGV) > 0)
{
  open(LFILE, "< $ARGV[0]") or die "Unable to open label file:  $ARGV[0]\n$!\n";
  while (my $line = <LFILE>)
  {
    chomp $line;
    push @alt_acc, $line;
  }
}
close LFILE;

$LABEL_SIZE = 8;  # All labels are padded or truncated to fit this size
$NUM_SPACES_AFTER_LABEL = 2;
$NUM_SPACES_BEFORE_NUMBER = 1;
$FIRST_FLAG = 1;
@acc = ();
@lines = ();
while ($line = <STDIN>)
{
  chomp $line;
  my @lineParts = split(/\s+/, $line);
  if (scalar(@alt_acc) > 0) {push @acc, shift @alt_acc;}
  else {push @acc, $lineParts[1];}
  if ($FIRST_FLAG == 1) {$lineParts[0] = lc($lineParts[0]);}
  else {$lineParts[0] = lc($lineParts[0]);}
  my @lineArray = split(//, $lineParts[0]);
  push @lines, \@lineArray;
  $FIRST_FLAG = 0;
}

$size = scalar(@{$lines[0]});
for ($i = 0; $i < $size; $i += 60)
{
  $mrnaBegin = $i + 1;
  $mrnaEnd = $i + 60;
  if ($mrnaEnd > $size) {$mrnaEnd = $size;}
  $acc_no = 0;
  foreach $line (@lines)
  {
    @printArray = splice(@{$line}, 0, 60);

    # Print label
    @labelArray = split(//, $acc[$acc_no]);
    for (my $i = 0; $i < $LABEL_SIZE; $i++)
    {
      if ($i >= scalar(@labelArray)) {print " ";}
      else {print $labelArray[$i];}
    }

    for (my $i = 0; $i < $NUM_SPACES_AFTER_LABEL; $i++) {print " ";}

    # Print sequence
    print @printArray;

    # Print number if line corresponds to the first sequence
    if ($acc_no == 0)
    {
      for (my $i = 0; $i < $NUM_SPACES_BEFORE_NUMBER; $i++) {print " ";}
      print $mrnaEnd;
    }
    print "\n";
    $acc_no++;
  }
  print "\n\n";
}
