#!/usr/bin/perl

#usage: patmrna.pl mrnafilename outfilename


#This program formats the mrna sequence file to the patent format

$infile = $ARGV[0];
$outfile = $ARGV[1];

if (!open(INFILE,"<$infile"))
{
    print "Could not open infile $infile\n";
    exit;
}
open(OUTFILE,">$outfile");


while ($line =<INFILE>)
{
    if ($line =~ /^>/)
    {}
    else
    {
	chomp($line);
	$line =~ s/ //g;
	$mrna_string .= $line;
    }
} #end while infile

@mrna_array = split("",$mrna_string);
$mrna_size = @mrna_array;
$pos_count=0;
$setnum = 0;
$group_count = 0;
for ($a=0; $a < $mrna_size; $a++)
{
    print OUTFILE $mrna_array[$a];
   
	$charcount++;
	$pos_count++;

    if ($pos_count == 10)
    {
	print OUTFILE " ";
	$pos_count = 0;
	$setnum++;
	$group_count++;
    }
    if ($charcount == 60)
    {
	$linecount += $charcount;
	print OUTFILE " $linecount\n\n";
	$charcount =0;
	$setnum = 0;
	$group_count = 0;
    }
    
} #end for loop

#char counts begin at pos 67

$linecount += $charcount;

for ($b=$charcount+$group_count; $b < 65; $b++)
{
    print OUTFILE " ";
}
print OUTFILE "  $linecount\n";
close(INFILE);
close(OUTFILE);
#system("rm -f -r $infile");

