#!/usr/bin/perl

#usage: patprot.pl protfilename outfilename


#This program formats the prot sequence file to the patent format

$infile = $ARGV[0];
$outfile = $ARGV[1];

if (!open(INFILE,"<$infile"))
{
    print "Could not open $infile\n";
    exit;
}

$line_count = 0;
$char_count = 0;
$prot_string = " ";
@prot_array = ();
@array_to_print = ();
@number_to_print = ();

if (!open(OUTFILE,">$outfile"))
{
    print "Could not open outfile $outfile\n";
    exit;
}
	
while ($line =<INFILE>)
{
    if ($line =~ /^>/)
    {
    }
    else
    {
	chomp($line);
	$line =~ s/ //g;
	$line =~ s/\*//g;
	$prot_string .= $line;
    }
} #end while infile
@prot_array = split("",$prot_string);
$prot_length = @prot_array;
push(@array_to_print,"");
push(@number_to_print,"");

for ($a=1; $a < $prot_length; $a++)
{
    $char_count++;
    $line_count++;
    push(@array_to_print,print_aa_code($prot_array[$a]));
    $pos = $char_count % 5;
    if ($char_count == 1000)
    {
	$char_count = 0;
    }
    
    if ($pos == 0 || $a == 1)
    {
	if ($char_count < 10)
	{
	    push(@number_to_print,"  $char_count");
	}
	elsif ($char_count < 100 && $char_count >= 10)
	{
	    push(@number_to_print," $char_count");
	}
	else
	{
	    push(@number_to_print,"$char_count");
	}
    }
    else
    {
	push(@number_to_print,"   ");
    }
}

# Always be sure to print the last character count, but make sure we haven't already stored it
if (($pos != 0) && ($a != 1))
{
    pop @number_to_print;
    if ($char_count < 10)
    {
        push(@number_to_print,"  $char_count");
    }
    elsif ($char_count < 100 && $char_count >= 10)
    {
        push(@number_to_print," $char_count");
    }
    else
    {
        push(@number_to_print,"$char_count");
    }
}

$pos_count = 0;
$print_size = @array_to_print;
$print_string ="";
$print_number = "";

for ($b=1; $b <= $print_size; $b++)
{
    $pos_count++;
    $print_string .= $array_to_print[$b]." ";
    $print_number .= $number_to_print[$b]." ";
    if ($pos_count == 16)
    {
	print OUTFILE "$print_string\n";
	print OUTFILE "$print_number\n\n";
	$pos_count = 0;
	$print_string ="";
	$print_number = "";
	
    }
}
print OUTFILE "$print_string\n";
	print OUTFILE "$print_number\n\n";
$print_string="";
$print_number="";
@array_to_print = ();
@number_to_print = ();
$char_count=0;
$line_count=0;
#system("rm -f -r $infile");




sub print_aa_code()
{
    $aa_code = $_[0];
    if ($aa_code eq "A")
    {
        return("Ala");
    }
    elsif ($aa_code eq "R")
    {
        return("Arg");
    }
    elsif ($aa_code eq "N")
    {
        return("Asn");
    }
    elsif ($aa_code eq "D")
    {
        return("Asp");
    }
    elsif ($aa_code eq "B")
    {
        return("Asx");
    }
    elsif ($aa_code eq "C")
    {
        return("Cys");
    }
    elsif ($aa_code eq "Q")
    {
        return("Gln");
    }
    elsif ($aa_code eq "E")
    {
        return("Glu");
    }
    elsif ($aa_code eq "Z")
    {
        return("Glx");
    }
    elsif ($aa_code eq "G")
    {
        return("Gly");
    }
    elsif ($aa_code eq "H")
      {
	  return("His");
      }
    elsif ($aa_code eq "I")
    {
        return("Ile");
    }
    elsif ($aa_code eq "L")
    {
        return("Leu");
    }
    elsif ($aa_code eq "K")
    {
        return("Lys");
    }
    elsif ($aa_code eq "M")
    {
        return("Met");
    }
    elsif ($aa_code eq "F")
    {
        return("Phe");
    }
    elsif ($aa_code eq "P")
    {
        return("Pro");
    }
    elsif ($aa_code eq "S")
    {
        return("Ser");
    }
    elsif ($aa_code eq "T")
    {
        return("Thr");
    }
    elsif ($aa_code eq "W")
    {
        return("Trp");
    }
    elsif ($aa_code eq "Y")
    {
        return("Tyr");
    }
    elsif ($aa_code eq "V")
    {
        return("Val");
    }
    
}

print OUTFILE "  $linecount\n";
close(INFILE);
close(OUTFILE);
