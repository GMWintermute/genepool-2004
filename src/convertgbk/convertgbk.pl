#!/usr/bin/perl
#
#This program calls GBK2EXP, GBK2SNP, and GBKSUP
#to create the neccessary support files for 
#genbank files.
#
#usage: convertgbk -i infile.gbk -o output_folder
#
#

if ($#ARGV < 3)
{
    print "========================================================\n";
    print "Please specify required arguements\n";
    print "\nUsage: convertgbk -i <infile.gbk> -o <output dir> (-D)\n";
    print "========================================================\n\n";
    print "convertgbk is a wrapper that runs the three following \n";
    print "programs:  gbk2exp <- Creates EXP formated outfile\n";
    print "           gbk2snp <- Creates snp file from gbk\n";
    print "           gbksup  <- Creates sup file for gbk\n\n";
    exit(1);
}

for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-o/)
    {
	$outdir = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile outfile: $outdir\n"; }


$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}
if ($DEBUG) { print "GENOMIX_GP: $genomix_gp\n"; }


#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
	if ($DEBUG) { print "gprootdir: $gprootdir \n";}
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
	if ($DEBUG) { print "gpdatadir $gpdatadir \n";}
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
	if ($DEBUG) { print "gpbuilddir $gpbuilddir \n";}
    }
    if ($line =~ /^gpsrc/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
	if ($DEBUG) { print "gpsrcdir: $gpsrcdir \n";}
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
	if ($DEBUG) { print "gbkdir: $gbkdir \n";}
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
	if ($DEBUG) { print "gpbin: $gpbin \n";}
    }

} #end of process cfg file
if ($DEBUG) { print "1 $gprootdir 2 $gpdatadir 3 $gpbuilddir 4 $gpsrcdir 5 $gbkdir 6 $gpbin\n"; }

close(TMPFILE);


#define the filename as NT*.gbk.snp
$outfile = $outdir."/".$infile.".snp";

#Convert GBK to snp files
$syscmd = "perl $gprootdir/src/gbk2snp/gbk2snp.pl $infile > $outfile";
if ($DEBUG) { print "$syscmd\n"; } 
$rc =0xffff & system($syscmd);
#if ($rc != 0) { die "execution of $syscmd failed"; }

#convert GBK to exp formatted out files

$outfile = $outdir."/".$infile.".exp";
if ($DEBUG) { print "gbk2exp outfile $outfile\n"; }

$syscmd = "$gpbin/gbk2exp $infile $outfile";
if ($DEBUG) { print "$syscmd\n"; } 
$rc =0xffff & system($syscmd);
#if ($rc != 0) { die "execution of $syscmd failed"; }


#convert GBK.exp to gbk.sup
$infile .= ".exp";
$snpfile = $infile;
$snpfile =~ s/\.exp/\.snp/g;
$outfile = $outdir."/".$infile.".sup";
$outfile =~ s/\.exp./\./g;
if ($DEBUG) { print "sup outfile: $outfile\n";}
$syscmd = "$gpbin/gbksup $infile $snpfile $outfile";
if ($DEBUG) { print "$syscmd\n"; } 
$rc =0xffff & system($syscmd);
if ($rc != 0) { die "execution of $syscmd failed"; }

#END of ConvertGBK












