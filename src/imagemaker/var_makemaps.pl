#!/usr/bin/perl

$PSFILE = $ARGV[0];
$build = $ARGV[1];
$chr_id = $ARGV[2];
if ($chr_id == 23)
{ $chr_id = "X"; }
if ($chr_id == 24)
{ $chr_id = "Y";}
if ($chr_id == "25")
{ $chr_id = "Un";}





$mapfile = $PSFILE;
$mapfile =~ s/.ps/.map/g;

$maptop =23;
open(PSFILE,"<$PSFILE");
open(MAPFILE,">$mapfile");
print STDERR "$mapfile\n";
while ($line = <PSFILE>)
{
    if ($line =~ /show/)
	{
	    if ($line =~ /Variant/)
	    {
#		
		$line =~ s/\) show//g;
		$line =~ s/\(//g;
		chomp($line);
		print $line."\n";
		@tmparray = split("- ",$line);

		print MAPFILE '<area shape="rect" coords="1,'.($maptop-20).',990,'.($maptop).'" ';
		print MAPFILE 'href="./gene.php?build='.$build.'&chr_id='.$chr_id.'&gene_id='.$tmparray[1].'">';
		print MAPFILE "\n";


	    }
	    elsif ($line =~ /Genbank/ && $line !~ /coding/)
	    {
		$line =~ s/\) show//g;
		$line =~ s/\(//g;
		chomp($line);
		print $line."\n";
		@tmparray = split("- ",$line);

		print MAPFILE '<area shape="rect" coords="1,'.($maptop-20).',990,'.($maptop).'" ';
		print MAPFILE 'href="javascript:openwin(\'./show_ncbi.php?reference='.$tmparray[1].'\')">';
		print MAPFILE "\n";
		
	    }
	    elsif ($line =! /Refseq/)
	    {}
	    elsif ($line =~ /Coding/)
	    {
#		print MAPFILE '<area shape = "rect" coords="1,'.$maptop.',990,'.($maptop+20).'" nohref>';
#		print MAPFILE "\n";

	    }
	    elsif ($line =~ /SNP/)
	    {
#                print STDERR "snp line found\n";
#		print MAPFILE '<area shape = "rect" coords="1,'.$maptop.',990,'.($maptop+20).'" nohref>';
#		print MAPFILE "\n";

	    }
	    else
	    {
                #the real work begins here
		$line =~ s/\) show//g;
		$line =~ s/\(//g;
		chomp($line);
		print $line."\n";
		@tmparray = split(" ",$line);
		print MAPFILE '<area shape="rect" coords="1,'.$maptop.',990,'.($maptop+20).'" ';
		print MAPFILE 'href="./show_ncbi.php?reference='.$tmparray[0].'">';
		print MAPFILE "\n";
	    }
	    $maptop +=20;



        } #end $line =~ /show/
}#end while
