#!/usr/bin/perl

#grab the Process Id number to use in creating tmp files
$pid = $$;


#grab the variables from the content_length string
#passed from the web browser

if ($ENV{'REQUEST_METHOD'} eq 'POST')
{
    #grab the input
    read(STDIN,$buffer,$ENV{'CONTENT_LENGTH'});
    #split the name-value pairs
    @pairs = split(/&/,$buffer);
}

#ok loop through the pairs and extract the fasta lines
#to blastdb.$pid

$tmpfile = "blastdb.$pid";
print "Content-type: text/html\n\n";
print "<HTML><HEAD></HEAD><body>";
print "<PRE>";
foreach $pair (@pairs)
{
    local($name,$value) = split(/=/,$pair);
    if ($name =~ /^evid/)
    {

#	print "extracting $value<BR>";
	$value =~ s/_gb//g;
	$syscmd = "setenv GENOMIXDB /genepool/genomixdb; /BACKUP/genepool/genomixdb/src/gbget/gbget_fasta.pl $value >> /genepool/tmp/".$tmpfile;
#	print "$syscmd<BR>";
	system($syscmd);
	$syscmd = "/usr/bin/perl /BACKUP/genepool/genomixdb/src/gbget/refseq_get_fasta.pl $value >> /genepool/tmp/".$tmpfile;
#	print "$syscmd\n";
	system($syscmd);

	$syscmd = "/BACKUP/genepool/genomixdb/gbget/pat_get_fasta.pl $value >> /genepool/tmp/".$tmpfile;
#	print "$syscmd<BR>";
	system($syscmd);

	$syscmd = "/BACKUP/genepool/genomixdb/gbget/swissprot_get_fasta.pl $value >> /genepool/tmp/".$tmpfile;
	system($syscmd);

	$syscmd = "/BACKUP/genepool/genomixdb/gbget/pat_prt_get_fasta.pl $value >> /genepool/tmp".$tmpfile;
	system($syscmd);


    }
    if ($name =~ /build/)
    {
	$build=$value;
    }
    if ($name =~ /chr_id/)
    {
	$chr_id=$value;
    }
    if ($name =~ /seq_id/)
    {
	$seq_id=$value;
    }
    if ($name =~ /gene_id/)
    {
	$gene_id=$value;
    }
    if ($name =~ /gene_num/)
    {
	$gene_num=$value;
    }
    if ($name =~ /var_num/)
    {
	$var_num=$value;
    }






} #end foreach pair
#print "Build: $build<br>";
#print "Chr_id: $chr_id<BR>";
#print "seq_id: $seq_id<BR>";
#print "Gene_id: $gene_id<BR>";
#print "gene_num: $gene_num<BR>";
#print "var_num: $var_num<BR>";


$syscmd = "/genepool/bin/formatdb -p F -t customdb -i /genepool/tmp/".$tmpfile;
#print "$syscmd\n";
system($syscmd);

$syscmd = "/genepool/bin/gxpindex /genepool/tmp/".$tmpfile;
#print "$syscmd\n";
system($syscmd);

$supfile = "/genepool/gpdata/builds/".$build."/symlinks/".$chr_id."/".$seq_id.".out.sup";
$fsafile = "/genepool/gpdata/gbkdir/symlinks/".$chr_id."/".$seq_id.".mfa.fsa";
#$fsafile = "/genepool/gpdata/builds/".$build."/symlinks/".$chr_id."/".$seq_id.".fsa";
$dblist = "/genepool/tmp/".$tmpfile;
open(OUTFILE,">/genepool/tmp/dblist.$pid");
print OUTFILE "$dblist\n";
close(OUTFILE);

$syscmd = "perl /genepool/src/est_align/est_align.pl $supfile $fsafile $gene_num $var_num /genepool/tmp/dblist.$pid";
#print "$syscmd<BR>";
system($syscmd);
