#!/usr/bin/perl


#make_exons.pl -i gp_filename -c chr_id



for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-i/)
    {
	$infile =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile dbase: $database chr_id: $chr_id\n"; }






$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}


#define the global variables
@raw_left=();
@raw_right=();
@single_exon_left=();
@single_exon_right=();
@index_left=();
@index_right=();
@tmparray=();
$gene_number ="";


#open the gpfile
if (!open(INFILE,"<$infile"))
{
     print "Could not open $infile\n";
    exit;
}

$previous_gene = "0";
$first = 1;
while ($line = <INFILE>)
{
   
    @raw_left=();                
    @raw_right=();
    @single_exon_left=();
    @single_exon_right=();
    @index_left=();
    @index_right=();
    @tmparray=();
    $gene_number="";


    $first = 1;
    process_gene();
    #sort_exon_table();
    build_master_list();
    print_exon_table();

    output_master_list();


}

################ END MAIN #######################
sub process_gene()
{
     #print "Resetting arrays\n";
    @raw_left=();
    @raw_right=();
    @single_exon_left=();
    @single_exon_right=();
    @index_left=();
    @index_right=();
    @tmparray=();
    $gene_number="";



    if ($line =~ /^Gene/)
    {
        #print $line;
        chomp($line);
        @tmparray = split(" ",$line);
        $gene_number_found = $tmparray[1];
        $gene_number = $tmparray[1];
        $gene_id = $tmparray[4];
        $variant = $tmparray[3];
        #print "gene_number_found: $gene_number_found\n Gene_id $gene_id\n";
        $stopme =0;
    }
    do
    {
	$line=<INFILE>;
	#print $line;
	if ($line =~ /^exon:/)
	{
	    @tmp3array = split(" ",$line);
	    $exon_exists = exon_exists($tmp3array[1],$tmp3array[2]);
	    #       print "\$exon_exists $exon_exists\n";
	    if ($exon_exists == 0)
	    {
		push(@raw_left,$tmp3array[1]);
		push(@raw_right,$tmp3array[2]);
                #print "gene: $gene_number var $variant: $tmp3array[1], $tmp3array[2]\n";
	    }
	}


    } while ($line !~ /^endgene/ && !eof(INFILE));

}

sub build_master_list()
{
    merge_exons();
    sort_exon_table();



}



sub merge_exons()
{
    $single_exon_left=();
    $single_exon_right=();
    #this routines leaves only distinct individual exons
    for ($d=0; $d <= $#raw_left; $d++)
    { #print "merge routine\n";
	# print "merge_exons ".$raw_left[$d].",". $raw_right[$d]."\n";
        if (check_exon($raw_left[$d], $raw_right[$d])== 1)
	{
	    #       print "Added Exon to single left\n";
            push(@single_exon_left,$raw_left[$d]);
            push(@single_exon_right, $raw_right[$d]);
	}
    }

}

sub check_exon()
{
    $exon_left_to_check = $_[0];
    $exon_right_to_check = $_[1];

    $exon_size = @exon_left;
    #print "Check_exon($exon_left_to_check,$exon_right_to_check)\n";
    for ($c = 0; $c <= $exon_size; $c++)
    {
        if ($exon_left_to_check == $single_exon_left[$c])
	{
            if ($exon_right_to_check == $single_exon_right[$c])
	    {
                return(0);
	    } #end right match
            else
	    { return(1); }
	} #end if left matches

    } #end for
    return(1);
} #end sub check_exon

sub sort_exon_table()
{

        @index_left=();
        @index_right=();
    #print "SORT_TABLE: Exon_left_num: ".$#single_exon_left."\n";
    @index_left = sort sort_left 0..$#single_exon_left;
    @index_right = sort sort_right 0..$#single_exon_right;



}

sub sort_right()
{
    $single_exon_right[$a] <=> $single_exon_right[$b];

}

sub sort_left()
{
    $single_exon_left[$a] <=> $single_exon_left[$b];
}



sub print_exon_table()
{
    #print "Master Exon Table\n";
    $acount=0;
  #  foreach $exon (@index_left)
  #  {   $acount++;
  #      print "$acount: Exon_left: ".$single_exon_left[$exon];
  #      print "  Exon_Right: ".$single_exon_right[$exon]."\n";
	#print OUTFILE "Number $exon Exon_left: ".$single_exon_left[$exon]."<BR>";
        #print OUTFILE "  Exon_Right: ".$single_exon_right[$exon]."<BR>";
#    }


} #end sub print_exon_table


sub exon_exists()
 {
     $left = $_[0];
     $right = $_[1];
    # print "sizeof raw_left: $#raw_left\n";
     for ($ee=0; $ee <= $#raw_left; $ee++)
     {
	 if ($raw_left[$ee] == $left && $raw_right[$ee] == $right)
	 {
	     #print "exon exitsts\n";
	     return 1; 
	 }
     }
     return 0;
     
 } #End exon_exists

sub output_master_list()
{
    $contig = $infile;

    $contig =~ s/.out.sup//;
    $outfile =  $contig;
    $outfile .= "_gene_".$gene_number.".master_exons";
    open(OUTFILE, ">$outfile");
    $acount=0;
    foreach $exon (@index_left)
    {   $acount++;
        print OUTFILE "$acount ".$single_exon_left[$exon];
        print OUTFILE " ".$single_exon_right[$exon]."\n";
	#print OUTFILE "Number $exon Exon_left: ".$single_exon_left[$exon]."<BR>";
        #print OUTFILE "  Exon_Right: ".$single_exon_right[$exon]."<BR>";
    }
    # print "Resetting arrays\n";
    @raw_left=();
    @raw_right=();
    @single_exon_left=();
    @single_exon_right=();
    @index_left=();
    @index_right=();
    @tmparray=();
    $gene_number="";





} #end output master list
