#!/usr/bin/perl
use POSIX;
use DBI();

#usage: ./mrnamaker.pl DBNAME CHR_ID



for ($a = 0; $a <= $#ARGV; $a++)
{
    if ($ARGV[$a] =~ /-t/)
    {
	$database =  $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-c/)
    {
	$chr_id = $ARGV[$a+1];
    }
    if ($ARGV[$a] =~ /-D/)
    {
	$DEBUG = 1;
    }
}

if ($DEBUG) { print "infile: $infile dbase: $database chr_id: $chr_id\n"; }






$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }
    if ($line =~ /^db_host/)
    {
	@tmparray = split("=",$line);
	$db_host = $tmparray[1];
    }
    if ($line =~ /^db_username/)
    {
	@tmparray = split("=",$line);
	$db_username = $tmparray[1];
    }
    if ($line =~ /^db_password/)
    {
	@tmparray = split("=",$line);
	$db_password = $tmparray[1];
    }
}



$mrnastring="";
#define and null the arrays;
@coding_left =();
@coding_right=();
@exon_left=();
@exon_right = ();
@tmparray = ();
@snp_coord = ();
@snp_active =();
@snp_original =();
@snp_new = ();





#################Main Routines Start here##############

#tell perl to use MYSQL

$genedb = DBI ->connect("DBI:mysql:database=$database;host=$db_hostname",$db_username,$db_password,{'RaiseError' => 1});
#$genedb = DBI ->connect("DBI:mysql:database=$database;host=$db_hostname",$db_username,$db_password);

#query the database for the selected chromosome and grab the GXDB, seq_id, gene_number, variant_number
#$sql = "select gene_id, seq_id, gene_number, variant_number from chr_".$chr_id."_summary where active = 0 order by gene_number, variant_number";
$sql = "select gene_id, seq_id, gene_number, variant_number from chr_".$chr_id."_summary where active = 0 order by gene_id";
print $sql."\n";
$sth=$genedb->prepare($sql);
$sth->execute();

#we got the query, lets load the arguements and process the genes
while (@ary=$sth->fetchrow_array())
{
    $gene_id = $ary[0];
    $seq_id = $ary[1];
    $gene_number = $ary[2];
    $variant_number = $ary[3];
    #   print $gene_id."\n";
    #  print $seq_id." ".$gene_number." ".$variant_number."\n";
    process_gene(\@coding_left, \@coding_right, \@exon_left, \@exon_right, \@tmparray,\@snp_coord, \@snp_active, \@snp_original, \@snp_new);
} #end while mysql result



sub process_gene()
{
#this loads all information about a particular gene.  Then it calls export_mrna

#    local $gene_id ;
#    local $seq_id ;
#    local $gene_number ;
#    local $variant_number;
#    local $chr_id ;
    
    #define and null the arrays;
    local    @coding_left =@{$_[0]};
    local    @coding_right=@{$_[1]};
    local    @exon_left=@{$_[2]};
    local @exon_right =@{$_[3]};
    local @tmparray = @{$_[4]};
    local    @snp_coord = @{$_[5]};
    local    @snp_active =@{$_[6]};
    local    @snp_original =@{$_[7]};
    local    @snp_new = @{$_[8]};
    
    
    
    $filename ="$gprootdir/inbound/symlinks/".$chr_id."/".$seq_id.".mrna";
    if (!open(MRNAFILE,"<$filename"))
    {
	print "Error opening $filename\n";
	exit;
    }
    $mrnastring="";
    
    #read the mrnafile and load the correct sequence
    while ($line = <MRNAFILE>)
    {
	if ($line =~ /$gene_id/)
	{
	    #print "Found $gene_id\n";
	    $line = <MRNAFILE>;
	    while ($line !~ /^>/ && ! eof MRNAFILE)
	    {
		chomp($line);
		$mrnastring .= $line;
		$line = <MRNAFILE>;
	    } #end while line ! fasta header
	} #end if gene_id
    } #end $line = MRNAFILE
    
#    print "MRNA: ".$mrnastring."\n";
    #print "size: ".length($mrnastring)."\n";
    
    close(MRNAFILE);
    
    #now we need to open the NT_XXXXX.out.sup file
    #and load coding, exons and snips
    
    $supfilename = "$gprootdir/inbound/symlinks/".$chr_id."/".$seq_id.".out.sup";
#   print $supfilename."\n";
    if (!open(SUPFILE,"<$supfilename"))
    {
	print "Couldn't open $supfilename\n";
	exit;
    }
    
    
    
    #   print "Entering supfile\n";
    #read in the sup file and load the arrays
    while ($line = <SUPFILE>)
    {
	#print "inside supfile\n";
	$compstring = "Gene ".$gene_number." Variant ".$variant_number;
	#print $compstring."\n";
	
	if ($line =~ /$compstring/)
	{
	    #   print "$line\n";
	    
	    $line = <SUPFILE>;
	    #    while ($line !~ /Gene/) #next gene statement is the trigger
	    do
	    {
		if ($line =~ /Gene/)
		{
		    last;
		}
		if ($line =~ /coding/ )
		{
		    @tmparray = split(" ",$line);
		    push(@coding_left,$tmparray[5]);
		    push(@coding_right,$tmparray[6]);
		    # print "CODING_right: ".$tmparray[6]."\n";
		} #end if coding
		
		if ($line =~ /^exon/)
		{
		    @tmparray = split(" ",$line);
		    push(@exon_left,$tmparray[5]);
		    push(@exon_right,$tmparray[6]);
		} #end if exon
		
		if ($line =~ /^snip/)
		{   #print "Found SNIP\n";
		    @tmparray = split(" ",$line);
		    push(@snp_coord,$tmparray[3]);
		    push(@snp_active,$tmparray[5]);
		    push(@snp_original,$tmparray[6]);
		    push(@snp_new,$tmparray[7]);
		} # end if snip
		

	    } while ($line = <SUPFILE>); #end while ! new gene
	    
	}  #end if line is correct gene variant number
    }#end while supfile
	
	#$coding_size = @coding_left;
#    for ($a =0; $a <= $coding_size-1; $a++)
#    {
#		print "Coding_left: ".$coding_left[$a]."\n";
#		print "Coding_right: ".$coding_right[$a]."\n";
#    } #end for a to coding size
#    $exon_size = @exon_left;
#    for ($a =0; $a <= $exon_size-1; $a++)
#    {
#		print "Exon_left ".$exon_left[$a]."\n";
#		print "exon_right ".$exon_right[$a]."\n";
#    } #end for a to exon size

	#ok now all the arrays are loaded.  Lets export this information
	export_mrna(\@snp_coord, \@snp_active, \@snp_original, \@snp_new, \@exon_left, \@exon_right, \@coding_left, \@coding_right, $mrnastring);

} #end SUB process_gene





#################Subs start here#######################

sub export_mrna()
{
#this creates the html code necessary for the mrna section
    local @snp_coord = @{$_[0]};
    local @snp_active = @{$_[1]};
    local @snp_original = @{$_[2]};
    local @snp_new = @{$_[3]};
    local @exon_left = @{$_[4]};
    local @exon_right = @{$_[5]};
    local @coding_left = @{$_[6]};
    local @coding_right = @{$_[7]};
    local $mrnastring = $_[8];
    
    #bust the mrnastring up into an mrnaarray
    $mrnastring2 = " ".$mrnastring;
    @mrna_array = split("",$mrnastring2);
    
    #now we are ready to traverse and output the mrna line.
    #stats are 60 chars per line with coding boundries, exon
    #boundries, and active/silent snips.

    #We need to initalize a few more vars first
    $mrnastring_counter = 0;
    $outputstring_counter = 0;
    $coding_on = 0;
    $exon_on = 0;
    @snp_to_print = ();
    $syscmd = "mkdir $gprootdir/inbound/symlinks/".$chr_id."/images/".$seq_id;
    system($syscmd);
    $syscmd = "mkdir $gprootdir/inbound/symlinks/".$chr_id."/images/".$seq_id."/mrna";
    system($syscmd);
    
    $aafilename = "$gprootdir/inbound/symlinks/".$chr_id."/images/".$seq_id."/mrna/".$gene_id.".mrna.html";
    
    if (!open(AAOUT,">$aafilename"))
    {
	print  "Error opening ".$aafilename."\n";
	exit;
    }
    else
    {
#	print  $aafilename." opened\n";
    }
    #print AAOUT ' <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><HTML><HEAD><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link rel=stylesheet type="text/css" href="../../../../../../code/global/genome.css"></HEAD><BODY background="#FF9966">';
    
    $snp_size = @snp_coord;
 #   print AAOUT "snip_size: ".$snp_size."<BR>";
#    for ($zz=0; $zz <= $snp_size; $zz++)
#    { print "here\n";
#      print AAOUT  "snp_coord".$snp_coord[$zz]."<BR>";
#      print AAOUT  "snp_active ".$snp_active[$zz]."<BR>";
#      print AAOUT  "snp_original ".$snp_original[$zz]."<BR>";
#      print  AAOUT "snp_new ".$snp_new[$zz]."<BR>";
#  }
    
    
    print AAOUT '<tt><font style="Courier"   color="#000000">';
    $cursor_pos = 0;
    $exon_type = 1;
    for ($a=0; $a <= length($mrnastring); $a++)
    {
	#print "in the mrnastring loop $a\n";
	$coding_result = is_coding(\@coding_left, \@coding_right, $a);
	$exon_result = is_exon(\@exon_left, \@exon_right, $a);
	$test_num_snips = @snp_coord;
	$snip_result = is_snip(\@snp_coord, $a);
	#check left positions  of all vars
	if ($coding_result == 1)
	{
	    $coding_on = 1;
	    if ($span_on == 1)
	    {
		print AAOUT "</span>";
		print AAOUT "<b>";
		print AAOUT $current_span;
		#       print "SPAN: ".$current_span."\n";
	    }
	    else
	    {
		print AAOUT "<b>";
	    }
	    #	print  "*Coding Found\n";
	}
	if ($snip_result >= 0)
	{
	    $snip_active = $snp_active[$snip_result];
	    $snip_original = $snp_original[$snip_result];
	    $snip_new = $snp_new[$snip_result];
	    
	    $snip_string .= "(".$snip_original.",".$snip_new.")";
	    
	    
	    
	    
	    
	    
	    if ($span_on == 1)
	    {
		print AAOUT '</span>';
		
		if ($coding_on == 1)
		{
		    print AAOUT "</b><b>";
		}
		if ($snip_active == 1)
		{
		    print AAOUT '</font>'.$current_span.'<u><font class="active" >';
		}
		else
		{
		    print AAOUT '</font>'.$current_span.'<U><font  class="inactive">';
		}
		#print AAOUT $current_span;
	    }
	    else
	    {    if ($coding_on == 1)
		 {
		     print AAOUT "<b>";
		 }
		 
		 if ($snip_active == 1)
		 {
		     print AAOUT '</font><u><font class="active" ><BR>';
		 }
		 else
		 {
		     print AAOUT '</font><u><font  class="inactive"><BR>';
		 }
		 
	     }
	}
	if ($exon_result == 1)
	{
	    if ($exon_type == 1)
	    {
		print AAOUT '<span class="exonedge2">';
		#		   print  "*Exon Found\n";
		$exon_type = 2;
		$span_on = 1;
		$current_span = '<span class="exonedge2">';
	    }
	    else
	    {
		print AAOUT '<span class="exonedge3">';
		$exon_type = 1;
		$span_on = 1;
		$current_span = '<span class="exonedge3">';
	    }
	}
	
	#print "*Coding Result: ".$coding_result."\n";
	#print "*Exon Result: ".$exon_result."\n";
	#print $mrna_array[$a];
	
	if ($snip_result >= 0)
	{
	    #print AAOUT lc($mrna_array[$a]);
	   # print AAOUT "$a: ";
	    print AAOUT $mrna_array[$a];
	   # print AAOUT ", $snip_result<BR>";

	}
	else
	{
#print AAOUT "$a: ";
	    print AAOUT $mrna_array[$a];
#print AAOUT ", $snip_result<BR>";
	}
	
	if ($snip_result >= 0)
	{
	    if ($span_on == 1)
	    {
                print AAOUT '</u></font></span>';
                #print AAOUT '</font>';
		print AAOUT $current_span;
		print AAOUT '<font color="#000000" >';
		
		
	    }
	    else
	    { print AAOUT '</font></u><font color="#000000" >';
	      
	  }
	}
	
	
	if ($coding_result == 2)
	{
	    if ($span_on == 1)
	    {
		print AAOUT "</span>";
		print AAOUT "</b>";
		$coding_on = 0;
		print AAOUT $current_span;
	    }
	    else
	    {
		print AAOUT '</b>';
		$coding_on = 0;
	    }
	    #print  "*coding end found\n";
	}
	if ($exon_result  == 2)
	{
	    print AAOUT "</span>";
	    # print  "*exon end found\n";
	    $span_on = 0;
	    
	}
	if ($cursor_pos == 60 )
	{
	    print AAOUT "</span></span></span></span>";
	    if ($coding_on == 1)
	    {print AAOUT "</b>";}
	    print AAOUT "&nbsp;&nbsp;&nbsp;".$snip_string."<BR>\n";
	    $cursor_pos = 0;
	    $snip_string ="";
	    if ($coding_on == 1)
	    {print AAOUT "<b>";}
	    print AAOUT "</span>";
	    print AAOUT $current_span;
	}
	$cursor_pos++;
    } #end for a to length of mrnastring
    print AAOUT "</span>";
    #print AAOUT "cursor_pos:" .$cursor_pos."snip_string: ".$snip_string." END";
    for ($zeb=$cursor_pos; $zeb <= 60; $zeb++)
    {
	print AAOUT "&nbsp;";
    }
    print AAOUT "&nbsp;&nbsp;&nbsp;".$snip_string."<BR>\n";
    
    
} #end sub export_mrna

sub is_exon()
{
    local @exon_left = @{$_[0]};
    local @exon_right = @{$_[1]};
    local $pos_to_compare = $_[2];
    
    $num_exons = @exon_left;
    for ($b=0; $b <= $num_exons-1; $b++)
    {
	#print "is_exon(".$pos_to_compare.",".$exon_left[$b].")\n";
	if ($pos_to_compare == $exon_left[$b])
	{ return(1); # Exon On
      }
	elsif ($pos_to_compare == $exon_right[$b])
	{ return(2); #exon off
      }
    }
    return(0); #not an exon
} #end sub is_exon

sub is_coding()
{
    local @coding_left = @{$_[0]};
    local @coding_right = @{$_[1]};
    local $pos_to_compare = $_[2];
    if ($pos_to_compare == $coding_left[0])
    { return(1); # Exon On
  }
    elsif ($pos_to_compare == $coding_right[0])
    { return(2); #exon off
  }
    
    return(0); #not an exon
} #end sub is_exon

sub is_snip()
{
    local @snp_coord = @{$_[0]};
    local $pos_to_compare = $_[1];
    
    $num_snips = @snp_coord;
    #print "123_num_snips: ".$num_snips."\n";
    for ($ab = 0; $ab <= $num_snips-1; $ab++)
    {  
	if ($pos_to_compare == 1486)
	{
	#print AAOUT "pos_to_cmp: ".$pos_to_compare." Compare: ".$snp_coord[$ab]."<BR>";
    }
	if ($pos_to_compare == $snp_coord[$ab])
	{# print AAOUT "returning snip pos: ".$pos_to_compare."<BR>";
            return($ab);
	}
    }
    return(-1);

}
