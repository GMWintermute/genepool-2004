#!/usr/bin/perl

#usage: patmrna.pl mrnafilename outfilename


#This program formats the mrna sequence file to the patent format

$infile = $ARGV[0];
#$outfile = $ARGV[1];

print "$infile $outfile\n";

if (!open(INFILE,"<$infile"))
{
    print "Could not open infile $infile\n";
    exit;
}

$linecount = 0;
$charcount = 0;
$mrna_string = " ";
@mrna_array = ();

while ($line =<INFILE>)
{
    if ($line =~ /^>/)
    {
	$line =~ s/^>//;
	
	@tmparray = split(" ",$line);
	$gxdb = @tmparray[0];
	@tmp2array = split("=", $tmparray[2]);
	$contig = $tmp2array[1];
	@tmp3array = split("=", $tmparray[1]);
	$chr_id = $tmp3array[1];
	
	
	$syscmd = "mkdir /home/ray/genepool/inbound/symlinks/".$chr_id."/images/".$contig."/patent";
	system($syscmd);
	
	$outfile = "/home/ray/genepool/inbound/symlinks/".$chr_id."/images/".$contig."/patent/".$gxdb.".patmrna";
	
	
	if (!open(OUTFILE,">$outfile"))
	{
	    print "Could not open outfile $outfile\n";
	    exit;
	}
	
    }
    else
    {
        chomp($line);
        $line =~ s/ //g;
        $mrna_string .= $line;
    }
} #end while infile

@mrna_array = split("",$mrna_string);
$mrna_size = @mrna_array;
$pos_count=0;
$setnum = 1;
$group_count = 0;
for ($a=1; $a <= $mrna_size; $a++)
{
    print OUTFILE $mrna_array[$a];
    $charcount++;
    $pos_count++;
    
    if ($pos_count == 10)
    {
        print OUTFILE " ";
        $pos_count = 0;
        $setnum++;
        $group_count++;
    }
    if ($charcount == 60)
    {
        $linecount += $charcount;
        print OUTFILE " $linecount\n\n";
        $charcount =0;
        $setnum = 1;
        $group_count = 0;
    }
    
} #end for loop

#char counts begin at pos 67

$linecount += $charcount;

print "line: $linecount  char: $charcount\n";
for ($b=$charcount+$group_count; $b <= 65; $b++)
{
    #print "$b\n";
    print OUTFILE " ";
}
print OUTFILE "  $linecount\n";
close(INFILE);
close(OUTFILE);
