#!/usr/bin/perl

use POSIX;

#this program splits the GBK files into complementary one meg
#portions that correspond to the contig 1meg files

$infile = $ARGV[0];
$bplimit = 986930;

open(INFILE,"<$infile");
@gene_number =();
@strand =();
@phase=();
@version=();
@exon_left=();
@exon_right=();
@converted_left=();
@converted_right=();
$left_most_exon =0;
$right_most_exon =0;
$coding_left =0;
$coding_right =0;
$mismatched_parts = 0;
@files_created=();
@exon_left_under=();
@exon_right_under=();
@exon_left_over=();
@exon_right_over=();


while ($line = <INFILE>)
{
    #check to see if there is a gene loaded.  If so print it out.
    #if not, reinit the arrays and load it.
    if ($line =~ /summary/)
    {
	if (@gene_number[0] ne "")
	{
	    print_out_gene();
	}
	@gene_number =();
	@strand =();
	@phase=();
	@version=();
	@exon_left=();
	@exon_right=();
	@converted_left=();
	@converted_right=();
	$left_most_exon =0;
	$right_most_exon =0;
	$coding_left =0;
	$coding_right =0;
	@exon_left_under=();
	@exon_right_under=();
	@exon_left_over=();
	@exon_right_over=();

	chomp ($line);
	@tmparray = split(" ",$line);
	push(@gene_number,$tmparray[0]);
	push(@strand,$tmparray[3]);
	push(@phase,$tmparray[4]);
	push(@version,$tmparray[11]);
	$coding_left= $tmparray[7];
	$coding_right = $tmparray[8];

    } #end if summary

    if ($line =~ /exon/)
    {
	chomp($line);
	@tmparray=split(" ",$line);
	push(@exon_left,$tmparray[3]);
	push(@exon_right,$tmparray[4]);
    } #end exon


} #end while

#call print_out_gene to get the last one
print_out_gene();
print "Mismatched parts: $mismatched_parts\n";

#subroutines start here
sub print_out_gene()
{
    @part_left=();
    @part_right=();
    
    for(my $a =0; $a<= $#exon_left; $a++)
    {
	$part_left[$a]=floor($exon_left[$a]/ $bplimit);
	$part_right[$a]=floor($exon_right[$a] / $bplimit);
	#print "part_left $part_left[$a] exon_left $exon_left[$a]\n";
	#print "part_right $part_right[$a] exon_right $exon_right[$a]\n";


    }

    #check to see that part numbers match
    #automatically assume all parts match until
    #the script finds out they dont.
    $all_parts_match = 1;

#################################
###Rework this to use coding boundarys
#################################
    $all_parts_match = 1;
    $coding_left_part = floor($coding_left / $bplimit);
    $coding_right_part = floor($coding_right / $bplimit);
    if ($coding_left_part != $coding_right_part)
    {
	$all_parts_match = 0;
	$mismatched_parts++;
    }


#    for(my $a =0; $a<= $#exon_left; $a++)
#    {
#	if ($part_left[$a] != $part_right[$a])
#	{
#	    $all_parts_match = 0;
#	}
#    }
    
    if ($all_parts_match == 1)
    {
	recalc_genes();
	print_exon();
    }
    else
    {
	do_mismatched();
    }

} #end print_out_gene



sub recalc_genes()
{
    for (my $a=0; $a <= $#exon_left; $a++)
    {
	$converted_left[$a] = $exon_left[$a] % $bplimit;
	$converted_right[$a] = $exon_right[$a] % $bplimit;
#	print "\n\n";
#	print "orig_left $exon_left[$a] part: $part_left[$a] new: $converted_left[$a]\n";
#	print "orig_right $exon_right[$a] part: $part_right[$a] new: $converted_right[$a]\n";
    }

} #end recalc genes

sub print_exon()
{
    $outfile = $infile;
    $partnum = $part_left[0]+1;
    $outfile .= ".test.".$partnum;
    $print_header =0;
    if (!-e $outfile)
    {
	$print_header=1;
    }
    open(OUTFILE,">>$outfile");
    
    if ($print_header == 1)
    {
	$print_header=0;
	print OUTFILE "begin genes\n";
    }
    find_leftmost_exon();
    find_rightmost_exon();
    $new_coding_left = $coding_left % $bplimit;
    $new_coding_right = $coding_right % $bplimit;
    print OUTFILE "$gene_number[0] 1 summary $strand[0] $phase[0] $leftmost_exon $rightmost_exon $new_coding_left $new_coding_right -1 -1 $version[0]\n";
    for (my $a=0; $a <= $#converted_left; $a++)
    {
	print OUTFILE "$gene_number[0] 1 exon $converted_left[$a] $converted_right[$a] -1 -1 -1 -1\n"; 
    }

} #end print_gene

sub find_leftmost_exon()
{
    $leftmost_exon = $converted_left[0];
    for (my $a=0; $a <= $#converted_left; $a++)
    {
	if ($converted_left[$a] <= $leftmost_exon)
	{
	    $leftmost_exon = $converted_left[$a];
	}
    }
	
} #end find_leftmost_exon

sub find_rightmost_exon()
{
    $rightmost_exon = $converted_right[0];
    for (my $a=0; $a <= $#converted_right; $a++)
    {
	if ($converted_right[$a] >= $rightmost_exon)
	{
	    $rightmost_exon = $converted_right[$a];
	}
    }
	

} #end find_rightmost_exon

sub do_mismatched()
{
    print "Mismatch $gene_number[0]\n";
    for (my $a=0; $a<= $#exon_left; $a++)
    {
	$mod_left = floor($exon_left[$a] / $bplimit);
	$mod_right = floor($exon_right[$a] / $bplimit);
	print "$a: $mod_left, $mod_right\n";
	if ($mod_left == 0 && $mod_right == 0)
	{
	    push(@exon_left_under,$exon_left[$a]);
	    push(@exon_right_under,$exon_right[$a]);
	}
	if ($mod_left > 0 && $mod_right > 0)
	{
	    push(@exon_left_over,$exon_left[$a]);
	    push(@exon_right_over,$exon_right[$a]);
	}
	if ($mod_left == 0 && $mod_right > 0)
	{
	    push(@exon_left_under,$exon_left[$a]);
	    push(@exon_right_under,986930);
	    push(@exon_left_over,986931);
	    push(@exon_right_over,$exon_right[$a]);
	}
    }
    print "Under Table\n==========\n";
    for (my $a=0; $a <= $#exon_left_under; $a++)
    {
	print "left: $exon_left_under[$a] right: $exon_right_under[$a]\n";
    }

    print "Over Table\n==========\n";
    for (my $a=0; $a <= $#exon_left_over; $a++)
    {
	print "left: $exon_left_over[$a] right: $exon_right_over[$a]\n";
    }
    
}
