#!/usr/bin/perl

$genomix_gp = $ENV{'GENOMIX_GP'};
if (!$genomix_gp)
{
    print "GENOMIX_GP environment variable not set\n";
    exit(1);
}

#open the genepool.cfg file, and initialize the vars
$cfgfile = $genomix_gp."/code/global/genepool.cfg";

if (!open(CFGFILE,"<$cfgfile"))
{
    print "Could not open CFGFILE $cfgfile\n";
    exit;
}

while ($line = <CFGFILE>)
{
    chomp($line);
    if ($line =~ /^gprootdir/)
    {
	@tmparray = split("=",$line);
	$gprootdir = $tmparray[1];
    }
    if ($line =~ /^gpdatadir/)
    {
	@tmparray = split("=",$line);
	$gpdatadir = $tmparray[1];
    }
    if ($line =~ /^gpbuilddir/)
    {
	@tmparray = split("=",$line);
	$gpbuilddir = $tmparray[1];
    }
    if ($line =~ /^gpsrcdir/)
    {
	@tmparray = split("=",$line);
	$gpsrcdir = $tmparray[1];
    }
    if ($line =~ /^gbkdir/)
    {
	@tmparray = split("=",$line);
	$gbkdir = $tmparray[1];
    }
    if ($line =~ /^gpbin/)
    {
	@tmparray = split("=",$line);
	$gpbin = $tmparray[1];
    }

}

for ($a = 1; $a <= 25; $a++)
{
    $chromonum = $a;
    if ($a == 23)
    {
	$chromonum = "X";
    }
    if ($a == 24)
    {
	$chromonum = "Y";
    }
    if ($a == 25)
    {
	$chromonum = "Un";
    }

    $folder = "./symlinks/$chromonum";
    print "Folder: $folder\n";
    opendir(MYDIR,$folder);
    @dirlist = readdir(MYDIR);
    closedir(MYDIR);
    #print "Numfiles $#dirlist\n";
    for ($b = 0; $b <= $#dirlist; $b++)
    {
	if ($dirlist[$b] =~ /^NT/ && $dirlist[$b] =~ /.out$/)
	{
	   # print "infile $dirlist[$b]\n";
	    $tmpfile = $dirlist[$b];
	    $tmpfile =~ s/.out/.gbk.snp/;
	    $filename = "$gbkdir/symlinks/$chromonum/$tmpfile";
	    #print "Filename to check: $filename\n";
	    if (-e $filename)
	    {
		print "file $filename exists\n";
	    }
	    else
	    {
		print "File $filename MISSING\n";
	    }
	} #end if mfa.fsa file

    } #end for file list



} #end for 1 to 25

