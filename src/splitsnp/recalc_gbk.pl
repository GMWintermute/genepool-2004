#!/usr/bin/perl

use POSIX;

#this program splits the GBK.exp files into complementary one meg
#portions that correspond to the contig 1meg files

$infile = $ARGV[0];
$bplimit = 986930;

open(INFILE,"<$infile");
@gene_number =();
@strand =();
@phase=();
@version=();
@exon_left=();
@exon_right=();
@converted_left=();
@converted_right=();
$left_most_exon =0;
$right_most_exon =0;
$coding_left =0;
$coding_right =0;
$mismatched_parts = 0;
@files_created=();
@exon_left_under=();
@exon_right_under=();
@exon_left_over=();
@exon_right_over=();
$print_convert = 0;

while ($line = <INFILE>)
{
    #check to see if there is a gene loaded.  If so print it out.
    #if not, reinit the arrays and load it.
    if ($line =~ /summary/)
    {
	if (@gene_number[0] ne "")
	{
	    print_out_gene();
	}
	@gene_number =();
	@strand =();
	@phase=();
	@version=();
	@exon_left=();
	@exon_right=();
	@converted_left=();
	@converted_right=();
	$left_most_exon =0;
	$right_most_exon =0;
	$coding_left =0;
	$coding_right =0;
	@exon_left_under=();
	@exon_right_under=();
	@exon_left_over=();
	@exon_right_over=();

	chomp ($line);
	@tmparray = split(" ",$line);
	push(@gene_number,$tmparray[0]);
	push(@strand,$tmparray[3]);
	push(@phase,$tmparray[4]);
	push(@version,$tmparray[11]);
	$coding_left= $tmparray[7];
	$coding_right = $tmparray[8];

    } #end if summary

    if ($line =~ /exon/)
    {
	chomp($line);
	@tmparray=split(" ",$line);
	push(@exon_left,$tmparray[3]);
	push(@exon_right,$tmparray[4]);
    } #end exon


} #end while

#call print_out_gene to get the last one
print_out_gene();
print "Mismatched parts: $mismatched_parts\n";

#subroutines start here
sub print_out_gene()
{
    @part_left=();
    @part_right=();
    
    for(my $a =0; $a<= $#exon_left; $a++)
    {
	$part_left[$a]=floor($exon_left[$a]/ $bplimit);
	$part_right[$a]=floor($exon_right[$a] / $bplimit);
	#print "part_left $part_left[$a] exon_left $exon_left[$a]\n";
	#print "part_right $part_right[$a] exon_right $exon_right[$a]\n";


    }

    #check to see that part numbers match
    #automatically assume all parts match until
    #the script finds out they dont.
    $all_parts_match = 1;

#################################
###Rework this to use coding boundarys
#################################
    $all_parts_match = 1;
    $coding_left_part = floor($coding_left / $bplimit);
    $coding_right_part = floor($coding_right / $bplimit);
    if ($coding_left_part != $coding_right_part)
    {
	$all_parts_match = 0;
	$mismatched_parts++;
    }


#    for(my $a =0; $a<= $#exon_left; $a++)
#    {
#	if ($part_left[$a] != $part_right[$a])
#	{
#	    $all_parts_match = 0;
#	}
#    }
    
    if ($all_parts_match == 1)
    {
	recalc_genes();
	print_exon();
    }
    else
    {
	log_gene();
	#$print_convert = 1;
	recalc_genes();
	$print_convert=0;
	do_mismatched();
    }

} #end print_out_gene



sub recalc_genes()
{
    for (my $a=0; $a <= $#exon_left; $a++)
    {
	$converted_left[$a] = $exon_left[$a] % $bplimit;
	$converted_right[$a] = $exon_right[$a] % $bplimit;
	if ($print_convert == 1)
	{
	    print "\n\n";
	    print "orig_left $exon_left[$a] part: $part_left[$a] new: $converted_left[$a]\n";
	    print "orig_right $exon_right[$a] part: $part_right[$a] new: $converted_right[$a]\n";
	}
	
    }

} #end recalc genes

sub print_exon()
{
    $outfile = $infile;
    $partnum = $part_left[0]+1;
    #$outfile .= ".test.".$partnum;
    $outfile =~ s/.gbk.exp//;
    $outfile .= ".".$partnum.".gbk.exp";



    $print_header =0;
    if (!-e $outfile)
    {
	$print_header=1;
    }
    open(OUTFILE,">>$outfile");
    
    if ($print_header == 1)
    {
	$print_header=0;
	print OUTFILE "begin genes\n";
    }
    find_leftmost_exon();
    find_rightmost_exon();
    $new_coding_left = $coding_left % $bplimit;
    $new_coding_right = $coding_right % $bplimit;
    print OUTFILE " $gene_number[0] 1 summary $strand[0] $phase[0] $leftmost_exon $rightmost_exon $new_coding_left $new_coding_right -1 -1 $version[0]\n";
    for (my $a=0; $a <= $#converted_left; $a++)
    {
	print OUTFILE " $gene_number[0] 1 exon $converted_left[$a] $converted_right[$a] -1 -1 -1 -1\n"; 
    }

} #end print_gene

sub find_leftmost_exon()
{
    $leftmost_exon = $converted_left[0];
    for (my $a=0; $a <= $#converted_left; $a++)
    {
	if ($converted_left[$a] <= $leftmost_exon)
	{
	    $leftmost_exon = $converted_left[$a];
	}
    }
	
} #end find_leftmost_exon

sub find_rightmost_exon()
{
    
    $rightmost_exon = $converted_right[0];
    for (my $a=0; $a <= $#converted_right; $a++)
    {
	if ($converted_right[$a] >= $rightmost_exon)
	{
	    $rightmost_exon = $converted_right[$a];
	}
    }
	

} #end find_rightmost_exon

sub do_mismatched()
{
    print "Mismatch $gene_number[0]\n";
    $count_reached = 0;
    $first_part_num=0;
    $second_part_num=0;

    for (my $a=0; $a<= $#converted_left; $a++)
    {
	print "part: $part_left[$a],$part_right[$a] $converted_left[$a],$converted_right[$a]\n";

	if ($count_reached == 1 && $converted_left[$a] < $converted_right[$a] )
	{
	    push(@exon_left_over,$converted_left[$a]);
	    push(@exon_right_over,$converted_right[$a]);
	}

	# tricky business now.  
	if ($count_reached == 0 && $converted_left[$a] > $converted_right[$a])
	{
	    push(@exon_left_under,$converted_left[$a]);
	    push(@exon_right_under,$bplimit);
	    push(@exon_left_over,1);
	    push(@exon_right_over,$converted_right[$a]);
	    $count_reached = 1;
	    $second_part_num = $part_right[$a];
	}
	
	# Flag the jump here
	if ( $converted_right[$a-1] > $converted_left[$a]&& $count_reached == 0 )
	{
	    $count_reached = 1;
	    push(@exon_left_over,$converted_left[$a]);
	    push(@exon_right_over,$converted_right[$a]);
	    $second_part_num = $part_left[$a];
	}
	
	
	if ($converted_right[$a] <= $bplimit && $count_reached == 0)
	{
	    push(@exon_left_under,$converted_left[$a]);
	    push(@exon_right_under,$converted_right[$a]);
	    $first_part_num=$part_left[$a];
	    
	}
    } #end for loop
    print "Under Table\n==========\n";
    for (my $a=0; $a <= $#exon_left_under; $a++)
    {
	print "left: $exon_left_under[$a] right: $exon_right_under[$a]\n";
    }
    
    print "Over Table\n==========\n";
    for (my $a=0; $a <= $#exon_left_over; $a++)
    {
	print "left: $exon_left_over[$a] right: $exon_right_over[$a]\n";
    }
    
    $upperleft = find_upper_leftmost();
    $upperright = find_upper_rightmost();
    $lowerleft = find_lower_leftmost();
    $lowerright = find_lower_rightmost();

    print "Lowermost $lowerleft,$lowerright\n";
    print "uppermost $upperleft,$upperright\n";







    $outfile = $infile;
    $partnum = $part_left[0]+1;
    #$outfile .= ".test.".$partnum;
    $outfile =~ s/.gbk.exp//;
    $outfile .= ".".$partnum.".gbk.exp";
   
    $print_header =0;
    if (!-e $outfile)
    {
	$print_header=1;
    }
    open(OUTFILE,">>$outfile");
    
    if ($print_header == 1)
    {
	$print_header=0;
	print OUTFILE "begin genes\n";
    }

    $new_coding_left = $coding_left % $bplimit;
    $new_coding_right = $coding_right % $bplimit;
    print OUTFILE "$gene_number[0] 1 summary $strand[0] $phase[0] $leftmost_exon $rightmost_exon $new_coding_left $bplimit -1 -1 $version[0] split\n";
    for (my $a=0; $a <= $#exon_left_under; $a++)
    {
	print OUTFILE "$gene_number[0] 1 exon $exon_left_under[$a] $exon_right_under[$a] -1 -1 -1 -1\n"; 
    }

    close(OUTFILE);


 
    

    $outfile = $infile;
    $partnum = $second_part_num+1;
 
    $outfile =~ s/.gbk.exp//;
    $outfile .= ".".$partnum.".gbk.exp";

    $print_header =0;
    if (!-e $outfile)
    {
	$print_header=1;
    }
    open(OUTFILE,">>$outfile");
    
    if ($print_header == 1)
    {
	$print_header=0;
	print OUTFILE "begin genes\n";
    }

    $new_coding_left = $coding_left % $bplimit;
    $new_coding_right = $coding_right % $bplimit;
    print OUTFILE "$gene_number[0] 1 summary $strand[0] $phase[0] $leftmost_exon $rightmost_exon 1 $new_coding_right  -1 -1 $version[0] split\n";
    for (my $a=0; $a <= $#exon_left_over; $a++)
    {
	print OUTFILE "$gene_number[0] 1 exon $exon_left_over[$a] $exon_right_over[$a] -1 -1 -1 -1\n"; 
    }

    close(OUTFILE);

    
	
    
} #end do_mismatched

sub find_upper_leftmost()
{
    $retval = $exon_left_over[0];
    for (my $a=0; $a <= $#exon_left_over; $a++)
    {
	if ($exon_left_over[$a] < $retval)
	{
	    $retval = $exon_left_over[$a];
	}
    }
    return $retval;
}

sub find_upper_rightmost()
{
    $retval = $exon_right_over[0];
    for (my $a=0; $a <= $#exon_right_over; $a++)
    {
	if ($exon_right_over[$a] > $retval)
	{
	    $retval = $exon_right_over[$a];
	}
    }
    return $retval;
}

sub find_lower_leftmost()
{
    $retval = $exon_left_under[0];
    for (my $a=0; $a <= $#exon_left_under; $a++)
    {
	if ($exon_left_under[$a] < $retval)
	{
	    $retval = $exon_left_under[$a];
	}
    }
    return $retval;
}

sub find_lower_rightmost()
{
    $retval = $exon_right_under[0];
    for (my $a=0; $a <= $#exon_right_under; $a++)
    {
	if ($exon_right_under[$a] > $retval)
	{
	    $retval = $exon_right_under[$a];
	}
    }
    return $retval;
}


sub log_gene()
{
    open(LOGFILE,">>LOGFILE");
    print LOGFILE "$infile: $version[0] split \n";
    close(LOGFILE);
}
