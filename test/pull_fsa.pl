#!/usr/bin/perl

open(INFILE,"<$ARGV[0]");
#open(OUTFILE,">$ARGV[0].fsa");

while ($line = <INFILE>)
{
    @tmparray = split(" ",$line);
    $gene_id = $tmparray[0];
    $seq_id = $tmparray[1];
    
    $mrnafile = "/genepool/gpdata/builds/confirmant5/CHR_21/".$seq_id.".prot";
    print "$seq_id, $gene_id, $mrnafile\n";
    open(MRNA,"<$mrnafile");
    while ($mrnaline = <MRNA>)
    {
	if ($mrnaline =~ /$gene_id/)
	{
	    $outfile = "$gene_id.fsa";

	    open(OUTFILE,">$outfile");
	    print OUTFILE $mrnaline;
	    
	    while ($mrnaline = <MRNA>)
	    {
		if ($mrnaline !~ /^>/)
		{
		    print OUTFILE $mrnaline;
		    #print "here  ";
		}
		else
		{
		    last;
		}
	    }
	    #close(OUTFILE);
	   
	}
    }
 close(MRNA);
}
